package sopra.vol;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Passager")
public class Passager {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "MAIL", length = 100)
	private String mail;
	@Column(name="TELEPHONE", length = 100)
	private String telephone;
	@Column(name="CIVILITE", length = 100)
	private Civilite civilite;
	@Column(name="NOM", length = 100)
	private String nom;
	@Column(name="PRENOM", length = 100)
	private String prenom;
	@Column(name="DATE_NAISSANCE")
	private Date dtNaissance;
	@Column(name="NATIONALITE", length = 100)
	private String nationalite;
	@Column(name="TYPE_PI", length = 100)
	private TypePieceIdentite typePI;
	@Column(name = "DATE_VALIDITE_PI")
	private Date dateValiditePI;
	@Embedded
	private Adresse principale;
	@OneToMany(mappedBy = "passager")
	private List<Reservation> reservations = new ArrayList<Reservation>();

	public Passager() {
		super();
	}

	public Passager(Long id, String mail, String telephone, Civilite civilite, String nom, String prenom,
			Date dtNaissance, String nationalite, TypePieceIdentite typePI, Date dateValiditePI) {
		super();
		this.id = id;
		this.mail = mail;
		this.telephone = telephone;
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
		this.dtNaissance = dtNaissance;
		this.nationalite = nationalite;
		this.typePI = typePI;
		this.dateValiditePI = dateValiditePI;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	@Enumerated(EnumType.STRING)
	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Date getDtNaissance() {
		return dtNaissance;
	}

	public void setDtNaissance(Date dtNaissance) {
		this.dtNaissance = dtNaissance;
	}

	public String getNationalite() {
		return nationalite;
	}

	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}

	public TypePieceIdentite getTypePI() {
		return typePI;
	}

	public void setTypePI(TypePieceIdentite typePI) {
		this.typePI = typePI;
	}

	public Date getDateValiditePI() {
		return dateValiditePI;
	}

	public void setDateValiditePI(Date dateValiditePI) {
		this.dateValiditePI = dateValiditePI;
	}

	public Adresse getPrincipale() {
		return principale;
	}

	public void setPrincipale(Adresse principale) {
		this.principale = principale;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
	
	

}
