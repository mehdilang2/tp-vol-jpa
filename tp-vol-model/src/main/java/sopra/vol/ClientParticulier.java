package sopra.vol;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;


@Entity
@DiscriminatorValue("ClientParticulier")
public class ClientParticulier extends Client {
	
	@Column(name = "CIVILITE", length = 50)
	private Civilite civilite;
	@Column(name = "NOM", length = 100)
	private String nom;
	@Column(name = "PRENOM", length = 100)
	private String prenom;

	public ClientParticulier() {
		super();
	}

	public ClientParticulier(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, Civilite civilite,
			String nom, String prenom) {
		super(id, mail, telephone, moyenPaiement);
		this.civilite = civilite;
		this.nom = nom;
		this.prenom = prenom;
	}
	
	@Enumerated(EnumType.STRING)
	public Civilite getCivilite() {
		return civilite;
	}

	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}
