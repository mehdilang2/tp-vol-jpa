package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Voyage")
public class Voyage {
	@Id
	@GeneratedValue
	private Long id;
	@OneToMany(mappedBy = "voyage")
	private List<Reservation> reservations = new ArrayList<Reservation>();
	@OneToMany(mappedBy = "voyage")
	private List<VoyageVol> vols = new ArrayList<VoyageVol>();

	public Voyage() {
		super();
	}

	public Voyage(Long id) {
		super();
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public List<VoyageVol> getVols() {
		return vols;
	}

	public void setVols(List<VoyageVol> vols) {
		this.vols = vols;
	}

}
