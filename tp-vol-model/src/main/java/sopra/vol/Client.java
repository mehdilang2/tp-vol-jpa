package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "CLIENT")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="TYPE_CLIENT")
public abstract class Client {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "MAIL", length = 100)
	private String mail;
	@Column(name = "TELEPHONE", length = 50)
	private String telephone;
	@Column(name = "MOYEN_PAIEMENT", length = 50)
	private MoyenPaiement moyenPaiement;
	@Embedded
//	@JoinColumns({
//        @JoinColumn(name = "voie",referencedColumnName="voie",insertable = false, updatable = false),
//        @JoinColumn(name = "complement",referencedColumnName="complement",insertable = false, updatable = false),
//        @JoinColumn(name = "codePostal",referencedColumnName="codepostal",insertable = false, updatable = false),
//        @JoinColumn(name = "ville",referencedColumnName="ville",insertable = false, updatable = false),
//        @JoinColumn(name = "pays",referencedColumnName="pays",insertable = false, updatable = false)
//    })
	private Adresse principale;
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name = "voie", column = @Column(name = "voie_facturation")),
		@AttributeOverride(name = "complement", column = @Column(name = "complement_facturation")),
		@AttributeOverride(name = "codePostal", column = @Column(name = "codePostal_facturation")),
		@AttributeOverride(name = "ville", column = @Column(name = "ville_facturation")),
		@AttributeOverride(name = "pays", column = @Column(name = "pays_facturation"))

	})
//	@JoinColumns({
//        @JoinColumn(name = "voie",referencedColumnName="voie",insertable = false, updatable = false),
//        @JoinColumn(name = "complement",referencedColumnName="complement",insertable = false, updatable = false),
//        @JoinColumn(name = "codePostal",referencedColumnName="codepostal",insertable = false, updatable = false),
//        @JoinColumn(name = "ville",referencedColumnName="ville",insertable = false, updatable = false),
//        @JoinColumn(name = "pays",referencedColumnName="pays",insertable = false, updatable = false)
//    })
	private Adresse facturation;
	@OneToMany(mappedBy = "client")
	private List<Reservation> reservations = new ArrayList<Reservation>();

	public Client() {
		super();
	}

	public Client(Long id, String mail, String telephone, MoyenPaiement moyenPaiement) {
		super();
		this.id = id;
		this.mail = mail;
		this.telephone = telephone;
		this.moyenPaiement = moyenPaiement;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	@Enumerated(EnumType.STRING)
	public MoyenPaiement getMoyenPaiement() {
		return moyenPaiement;
	}

	public void setMoyenPaiement(MoyenPaiement moyenPaiement) {
		this.moyenPaiement = moyenPaiement;
	}

	public Adresse getPrincipale() {
		return principale;
	}

	public void setPrincipale(Adresse principale) {
		this.principale = principale;
	}

	public Adresse getFacturation() {
		return facturation;
	}

	public void setFacturation(Adresse facturation) {
		this.facturation = facturation;
	}

	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

}
