package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "AEROPORT")
public class Aeroport {
	
	@Id
	@Column(name="CODE")
	private String code;
	@ManyToMany
	@JoinTable(
			name = "AEROPORT_VILLE",
			uniqueConstraints=@UniqueConstraint(columnNames={ "AEROPORT_ID", "VILLE_ID" }),
			joinColumns=@JoinColumn(name="AEROPORT_ID"),
			inverseJoinColumns=@JoinColumn(name="VILLE_ID")
			)
	private List<Ville> villes = new ArrayList<Ville>();

	public Aeroport() {
		super();
	}

	public Aeroport(String code) {
		super();
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public List<Ville> getVilles() {
		return villes;
	}

	public void setVilles(List<Ville> villes) {
		this.villes = villes;
	}

}
