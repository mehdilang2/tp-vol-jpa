package sopra.vol;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@DiscriminatorValue("ClientPro")
public class ClientPro extends Client {
	@Column(name = "NUMERO_SIRET", length = 100)
	private String numeroSiret;
	@Column(name = "NOM_ENTREPRISE", length = 100)
	private String nomEntreprise;
	@Column(name = "NUM_TVA", length = 100)
	private String numTVA;
	@Column(name = "TYPE_ENTREPRISE", length = 100)
	private TypeEntreprise typeEntreprise;

	public ClientPro() {
		super();
	}

	public ClientPro(Long id, String mail, String telephone, MoyenPaiement moyenPaiement, String numeroSiret,
			String nomEntreprise, String numTVA, TypeEntreprise typeEntreprise) {
		super(id, mail, telephone, moyenPaiement);
		this.numeroSiret = numeroSiret;
		this.nomEntreprise = nomEntreprise;
		this.numTVA = numTVA;
		this.typeEntreprise = typeEntreprise;
	}

	public String getNumeroSiret() {
		return numeroSiret;
	}

	public void setNumeroSiret(String numeroSiret) {
		this.numeroSiret = numeroSiret;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getNumTVA() {
		return numTVA;
	}

	public void setNumTVA(String numTVA) {
		this.numTVA = numTVA;
	}

	@Enumerated(EnumType.STRING)
	public TypeEntreprise getTypeEntreprise() {
		return typeEntreprise;
	}

	public void setTypeEntreprise(TypeEntreprise typeEntreprise) {
		this.typeEntreprise = typeEntreprise;
	}

}
