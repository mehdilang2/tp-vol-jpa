package sopra.vol;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Compagnie")
public class Compagnie {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "NOM_COMPAGNIE", length = 100)
	private String nomCompagnie;
	@OneToMany(mappedBy="compagnie")
	private List<Vol> vols = new ArrayList<Vol>();

	public Compagnie() {
		super();
	}

	public Compagnie(Long id, String nomCompagnie) {
		super();
		this.id = id;
		this.nomCompagnie = nomCompagnie;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNomCompagnie() {
		return nomCompagnie;
	}

	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}

	public List<Vol> getVols() {
		return vols;
	}

	public void setVols(List<Vol> vols) {
		this.vols = vols;
	}

}
