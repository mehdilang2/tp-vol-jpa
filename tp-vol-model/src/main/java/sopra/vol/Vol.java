package sopra.vol;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="Vol")
public class Vol {
	@Id
	@GeneratedValue
	private Long id;
	@Column(name = "NUMERO", length = 100)
	private String numero;
	@Column(name = "DATE_DEPART")
	private Date dateDepart;
	@Column(name = "DATEARRIVEE")
	private Date dateArrivee;
	@Column(name = "OUVERT", length = 50)
	private boolean ouvert;
	@Column(name = "NB_PLACES", length = 100)
	private int nbPlaces;
	@OneToMany(mappedBy = "vol")
	private List<VoyageVol> voyages = new ArrayList<VoyageVol>();
	@ManyToOne
	@JoinColumn(name="COMPAGNIE")
	private Compagnie compagnie;
	@ManyToOne
	@JoinColumn(name="AEROPORT_DEPART")
	private Aeroport depart;
	@ManyToOne
	@JoinColumn(name="AEROPORT_ARRIVEE")
	private Aeroport arrivee;

	public Vol() {
		super();
	}

	public Vol(Long id, String numero, Date dateDepart, Date dateArrivee, boolean ouvert, int nbPlaces) {
		super();
		this.id = id;
		this.numero = numero;
		this.dateDepart = dateDepart;
		this.dateArrivee = dateArrivee;
		this.ouvert = ouvert;
		this.nbPlaces = nbPlaces;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getDateDepart() {
		return dateDepart;
	}

	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}

	public Date getDateArrivee() {
		return dateArrivee;
	}

	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}

	public boolean isOuvert() {
		return ouvert;
	}

	public void setOuvert(boolean ouvert) {
		this.ouvert = ouvert;
	}

	public int getNbPlaces() {
		return nbPlaces;
	}

	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	public List<VoyageVol> getVoyages() {
		return voyages;
	}

	public void setVoyages(List<VoyageVol> voyages) {
		this.voyages = voyages;
	}

	public Compagnie getCompagnie() {
		return compagnie;
	}

	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}

	public Aeroport getDepart() {
		return depart;
	}

	public void setDepart(Aeroport depart) {
		this.depart = depart;
	}

	public Aeroport getArrivee() {
		return arrivee;
	}

	public void setArrivee(Aeroport arrivee) {
		this.arrivee = arrivee;
	}

}
