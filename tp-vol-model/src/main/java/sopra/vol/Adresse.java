package sopra.vol;

import javax.persistence.Embeddable;

@Embeddable 
public class Adresse {
//	@Column(name = "VOIE", length = 150)
	private String voie;
//	@Column(name = "COMPLEMENT", length = 150)
	private String complement;
//	@Column(name = "CODE_POSTAL", length = 5)
	private String codePostal;
//	@Column(name = "VILLE", length = 100)
	private String ville;
//	@Column(name = "PAYS", length = 50)
	private String pays;

	public Adresse() {
		super();
	}

	public Adresse(String voie, String complement, String codePostal, String ville, String pays) {
		super();
		this.voie = voie;
		this.complement = complement;
		this.codePostal = codePostal;
		this.ville = ville;
		this.pays = pays;
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

	public String getComplement() {
		return complement;
	}

	public void setComplement(String complement) {
		this.complement = complement;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}

}
