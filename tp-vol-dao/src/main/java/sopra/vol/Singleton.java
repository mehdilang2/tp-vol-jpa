package sopra.vol;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import sopra.vol.dao.IAeroportDao;
import sopra.vol.dao.IClientDao;
import sopra.vol.dao.ICompagnieDao;
import sopra.vol.dao.IPassagerDao;
import sopra.vol.dao.IReservationDao;
import sopra.vol.dao.IVilleDao;
import sopra.vol.dao.IVolDao;
import sopra.vol.dao.IVoyageDao;
import sopra.vol.dao.IVoyageVolDao;
import sopra.vol.dao.jpa.AeroportDaoJpa;
import sopra.vol.dao.jpa.ClientDaoJpa;
import sopra.vol.dao.jpa.CompagnieDaoJpa;
import sopra.vol.dao.jpa.PassagerDaoJpa;
import sopra.vol.dao.jpa.ReservationDaoJpa;
import sopra.vol.dao.jpa.VilleDaoJpa;
import sopra.vol.dao.jpa.VolDaoJpa;
import sopra.vol.dao.jpa.VoyageDaoJpa;
import sopra.vol.dao.jpa.VoyageVolDaoJpa;

public class Singleton {
	private static Singleton instance = null;
	
	private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("vol-tp");

	private final IAeroportDao aeroportDao = new AeroportDaoJpa();
	private final IClientDao clientDao = new ClientDaoJpa();
	private final ICompagnieDao compagnieDao = new CompagnieDaoJpa();
	private final IPassagerDao passagerDao = new PassagerDaoJpa();
	private final IReservationDao reservationDao = new ReservationDaoJpa();
	private final IVilleDao villeDao = new VilleDaoJpa();
	private final IVolDao volDao = new VolDaoJpa();
	private final IVoyageDao voyageDao = new VoyageDaoJpa();
	private final IVoyageVolDao voyageVolDao = new VoyageVolDaoJpa();

	private Singleton() {

	}

	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}

		return instance;
	}
	
	public EntityManagerFactory getEmf() {
		return emf;
	}

	public IAeroportDao getAeroportDao() {
		return aeroportDao;
	}
	
	public IClientDao getClientDao() {
		return clientDao;
	}
	
	public ICompagnieDao getCompagnieDao() {
		return compagnieDao;
	}
	
	public IPassagerDao getPassagerDao() {
		return passagerDao;
	}
	
	public IReservationDao getReservationDao() {
		return reservationDao;
	}
	
	public IVilleDao getVilleDao() {
		return villeDao;
	}
	
	public IVolDao getVolDao() {
		return volDao;
	}
	
	public IVoyageDao getVoyageDao() {
		return voyageDao;
	}
	
	public IVoyageVolDao getVoyageVolDao() {
		return voyageVolDao;
	}
}
