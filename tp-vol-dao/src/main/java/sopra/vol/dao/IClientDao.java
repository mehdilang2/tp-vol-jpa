package sopra.vol.dao;

import java.util.List;

import sopra.vol.Client;
import sopra.vol.ClientParticulier;
import sopra.vol.ClientPro;

public interface IClientDao extends IDao<Client, Long>{
	List<ClientParticulier> findAllClientParticulier();
	List<ClientPro> findAllClientPro();
}
