package sopra.vol;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import sopra.vol.Singleton;
import sopra.vol.dao.IAeroportDao;
import sopra.vol.dao.IClientDao;
import sopra.vol.dao.ICompagnieDao;
import sopra.vol.dao.IPassagerDao;
import sopra.vol.dao.IReservationDao;
import sopra.vol.dao.IVilleDao;
import sopra.vol.dao.IVolDao;
import sopra.vol.dao.IVoyageDao;
import sopra.vol.dao.IVoyageVolDao;

public class TestJpa {

	public static void main(String[] args) {
		
		IAeroportDao aeroportDao = Singleton.getInstance().getAeroportDao();
		IClientDao clientDao = Singleton.getInstance().getClientDao();
		ICompagnieDao compagnieDao = Singleton.getInstance().getCompagnieDao();
		IPassagerDao passagerDao = Singleton.getInstance().getPassagerDao();
		IReservationDao reservationDao = Singleton.getInstance().getReservationDao();
		IVilleDao villeDao = Singleton.getInstance().getVilleDao();
		IVolDao volDao = Singleton.getInstance().getVolDao();
		IVoyageDao voyageDao = Singleton.getInstance().getVoyageDao();
		IVoyageVolDao voyageVolDao = Singleton.getInstance().getVoyageVolDao();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Ville paris = new Ville();
		Ville londres = new Ville();
		Ville barcelone = new Ville();
		
		paris.setNom("Paris");
		paris = villeDao.save(paris);
		
		londres.setNom("Londres");
		londres = villeDao.save(londres);
		
		barcelone.setNom("Barcelone");
		barcelone = villeDao.save(barcelone);
		
		List<Ville> cdg = new ArrayList<Ville>();
		cdg.add(paris);
		
		List<Ville> ldn = new ArrayList<Ville>();
		ldn.add(londres);
		
		List<Ville> bcln = new ArrayList<Ville>();
		ldn.add(barcelone);
		
		
		Aeroport parisAeroport = new Aeroport();
		Aeroport londresAeroport = new Aeroport();
		Aeroport barceloneAeroport = new Aeroport();
		
		parisAeroport.setCode("CDG");
		parisAeroport.setVilles(cdg);
		
		parisAeroport = aeroportDao.save(parisAeroport);
		
		londresAeroport.setCode("LDN");
		londresAeroport.setVilles(ldn);
		
		londresAeroport = aeroportDao.save(londresAeroport);
		
		barceloneAeroport.setCode("BCL");
		barceloneAeroport.setVilles(bcln);
		
		barceloneAeroport = aeroportDao.save(barceloneAeroport);
		
		List<Aeroport> aeroportsParis = new ArrayList<Aeroport>();
		aeroportsParis.add(parisAeroport);
		
		List<Aeroport> aeroportsLondres = new ArrayList<Aeroport>();
		aeroportsLondres.add(londresAeroport);
		
		List<Aeroport> aeroportsBarcelone = new ArrayList<Aeroport>();
		aeroportsBarcelone.add(barceloneAeroport);
		
		paris.setAeroports(aeroportsParis);
		
		paris = villeDao.save(paris);
		
		londres.setAeroports(aeroportsLondres);
		
		londres = villeDao.save(londres);
		
		barcelone.setAeroports(aeroportsBarcelone);
		
		barcelone = villeDao.save(barcelone);
		
		Compagnie airFrance = new Compagnie();
		Compagnie royalLondon = new Compagnie();
		
		airFrance.setNomCompagnie("Air France");
		royalLondon.setNomCompagnie("Royal London");
		
		royalLondon = compagnieDao.save(royalLondon);
		airFrance = compagnieDao.save(airFrance);
		
		Voyage parisLondres = new Voyage();
		Voyage londresParis = new Voyage();
		
		londresParis = voyageDao.save(londresParis);
		parisLondres = voyageDao.save(parisLondres);
		
		Vol PL36 = new Vol();
		Vol LB45 = new Vol();
		Vol BP45 = new Vol();
		
		
		
		PL36.setCompagnie(airFrance);
		try {
			PL36.setDateDepart(sdf.parse("09/04/2019"));
			PL36.setDateArrivee(sdf.parse("09/04/2019"));
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		PL36.setDepart(parisAeroport);
		PL36.setArrivee(londresAeroport);
		PL36.setNbPlaces(34);
		PL36.setNumero("PL36");
		PL36.setOuvert(true);
		
		
		
		LB45.setCompagnie(royalLondon);
		
		try {
			LB45.setDateDepart(sdf.parse("10/05/2019"));
			LB45.setDateArrivee(sdf.parse("10/05/2019"));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		LB45.setDepart(londresAeroport);
		LB45.setArrivee(barceloneAeroport);
		LB45.setNbPlaces(56);
		LB45.setNumero("LB45");
		LB45.setOuvert(false);
		
		BP45.setCompagnie(royalLondon);
		try {
			BP45.setDateDepart(sdf.parse("10/05/2019"));
			BP45.setDateArrivee(sdf.parse("11/05/2019"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		BP45.setDepart(barceloneAeroport);
		BP45.setArrivee(parisAeroport);
		BP45.setNbPlaces(17);
		BP45.setNumero("BP45");
		BP45.setOuvert(false);
		
		PL36 = volDao.save(PL36);
		LB45 = volDao.save(LB45);
		BP45 = volDao.save(BP45);
		
		List<Vol> airFranceVols = new ArrayList<Vol>();
		airFranceVols.add(PL36);
		
		List<Vol> royalLondonVols = new ArrayList<Vol>();
		royalLondonVols.add(LB45);
		royalLondonVols.add(BP45);
		
		
		
		VoyageVol parisLondresPL36 = new VoyageVol();
		VoyageVol londresBarceloneLB45 = new VoyageVol();
		VoyageVol barceloneParisLB45 = new VoyageVol();
		
		parisLondresPL36.setOrdre(1);
		parisLondresPL36.setVol(PL36);
		parisLondresPL36.setVoyage(parisLondres);
		
		londresBarceloneLB45.setOrdre(1);
		londresBarceloneLB45.setVol(LB45);
		londresBarceloneLB45.setVoyage(londresParis);
		
		barceloneParisLB45.setOrdre(2);
		barceloneParisLB45.setVol(BP45);
		barceloneParisLB45.setVoyage(londresParis);
		
		List<VoyageVol> parisLondresVV = new ArrayList<VoyageVol>();
		parisLondresVV.add(parisLondresPL36);
		
		List<VoyageVol> londresBarceloneVV = new ArrayList<VoyageVol>();
		londresBarceloneVV.add(londresBarceloneLB45);
		
		List<VoyageVol> barceloneParisVV = new ArrayList<VoyageVol>();
		barceloneParisVV.add(barceloneParisLB45);
		
		PL36.setVoyages(parisLondresVV);
		LB45.setVoyages(londresBarceloneVV);
		BP45.setVoyages(barceloneParisVV);
		parisLondres.setVols(parisLondresVV);
		londresParis.setVols(londresBarceloneVV);
		airFrance.setVols(airFranceVols);
		royalLondon.setVols(royalLondonVols);
		
		barceloneParisLB45=voyageVolDao.save(barceloneParisLB45);
		londresBarceloneLB45=voyageVolDao.save(londresBarceloneLB45);
		parisLondresPL36=voyageVolDao.save(parisLondresPL36);
		
		Reservation res1 = new Reservation();
        Reservation res2 = new Reservation();
        
        res1.setNumero("Res1");
        res1.setStatut(true);
        res1.setTarif(664.9f);
        res1.setTauxTVA(1f);
        res1.setVoyage(parisLondres);
        try {
            res1.setDateReservation(sdf.parse("01/01/2019"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        res1 = reservationDao.save(res1);
        
        res2.setNumero("Res2");
        res2.setStatut(true);
        res2.setTarif(700.9f);
        res2.setTauxTVA(1f);
        res2.setVoyage(londresParis);
        try {
            res2.setDateReservation(sdf.parse("02/01/2019"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        
        res2 = reservationDao.save(res2);
        
        
        Adresse adresseThomas = new Adresse();
        Adresse adresseMehdi = new Adresse();
        Adresse adresseSopra = new Adresse();
        
        adresseThomas.setVoie("137 rue de Kater");
        adresseThomas.setComplement("Appartement A11");
        adresseThomas.setCodePostal("33000");
        adresseThomas.setVille("Bordeaux");
        adresseThomas.setPays("France");
        
        adresseMehdi.setVoie("6 rue de l'inspiration");
        adresseMehdi.setComplement(null);
        adresseMehdi.setCodePostal("33000");
        adresseMehdi.setVille("Bordeaux");
        adresseMehdi.setPays("France");
        
        adresseSopra.setVoie("69 rue Pythagore");
        adresseSopra.setComplement(null);
        adresseSopra.setCodePostal("33100");
        adresseSopra.setVille("Mérignac");
        adresseSopra.setPays("France");
        
        
        
        
        ClientParticulier thomasParticulier = new ClientParticulier();
        ClientPro sopraPro = new ClientPro();
        
        thomasParticulier.setMail("bastont@hotmail.fr");
        thomasParticulier.setTelephone("0670272190");
        thomasParticulier.setMoyenPaiement(MoyenPaiement.CB);
        thomasParticulier.setCivilite(Civilite.M);
        thomasParticulier.setNom("Baston");
        thomasParticulier.setPrenom("Thomas");
        thomasParticulier.getReservations().add(res1);
        thomasParticulier.setPrincipale(adresseThomas);
        thomasParticulier.setFacturation(adresseSopra);
        
        thomasParticulier = (ClientParticulier) clientDao.save(thomasParticulier);
        
        sopraPro.setMail("Mail@Sopra.fr");
        sopraPro.setTelephone("0612345678");
        sopraPro.setMoyenPaiement(MoyenPaiement.VIREMENT);
        sopraPro.setNumeroSiret("669");
        sopraPro.setNomEntreprise("Sopra Steria");
        sopraPro.setNumTVA("36");
        sopraPro.setTypeEntreprise(TypeEntreprise.SARL);
        sopraPro.getReservations().add(res2);
        sopraPro.setFacturation(adresseSopra);
        
        sopraPro = (ClientPro) clientDao.save(sopraPro);
        
        
        
        
        Passager thomasPassager = new Passager();
        Passager mehdiPassager = new Passager();
        
        thomasPassager.setMail("bastont@hotmail.fr");
        thomasPassager.setTelephone("0670272190");
        thomasPassager.setNom("Baston");
        thomasPassager.setPrenom("Thomas");
        try {
            thomasPassager.setDtNaissance(sdf.parse("06/04/1992"));
            thomasPassager.setDateValiditePI(sdf.parse("06/04/2021"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        thomasPassager.setNationalite("Français");
        thomasPassager.setCivilite(Civilite.M);
        thomasPassager.setTypePI(TypePieceIdentite.PASSEPORT);
        thomasPassager.getReservations().add(res1);
        thomasPassager.setPrincipale(adresseThomas);
        
        thomasPassager = passagerDao.save(thomasPassager);
        
        mehdiPassager.setMail("langm@hotmail.fr");
        mehdiPassager.setTelephone("0606060606");
        mehdiPassager.setNom("Lang");
        mehdiPassager.setPrenom("Mehdi");
        try {
            mehdiPassager.setDtNaissance(sdf.parse("20/02/1994"));
            mehdiPassager.setDateValiditePI(sdf.parse("20/02/2023"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        mehdiPassager.setNationalite("Français");
        mehdiPassager.setCivilite(Civilite.M);
        mehdiPassager.setTypePI(TypePieceIdentite.CARTE_IDENTITE);
        mehdiPassager.getReservations().add(res2);
        mehdiPassager.setPrincipale(adresseMehdi);
        
        mehdiPassager = passagerDao.save(mehdiPassager);
		
		
		
		


	}

}
